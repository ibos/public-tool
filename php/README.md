# 一些学习资料

----

### [awesome-yii2](https://github.com/forecho/awesome-yii2.git)

### [awesome-javascript](https://github.com/sorrycc/awesome-javascript.git)

### [awesome-php](https://github.com/ziadoz/awesome-php.git)

### [fetool(前端工具)](https://github.com/nieweidong/fetool.git)

### [manong(码农)](https://github.com/nemoTyrant/manong.git)

### [php-fig-standards](https://github.com/jifei/php-fig-standards.git)

### [高并发redis+mysql+php](http://segmentfault.com/a/1190000004136250)